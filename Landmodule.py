from pystac_client import Client

from shapely.geometry import shape
import pyproj
from shapely.ops import transform
import geopandas

import numpy as np
import pandas as pd
from tensorflow.keras.utils import normalize

import rioxarray
import rasterio
import folium
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler

import tensorflow as tf
from tensorflow.keras.utils import normalize
from sklearn.impute import KNNImputer

from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans


# Function
def get_satellite_img(lat, lon, datetime):

    # Defining url of the aws storage where data is stored
    api_url = "https://earth-search.aws.element84.com/v0"
    client = Client.open(api_url)

    # Setting up the name of the collection we want to access: Sentinel-2, Level 2A, COGs
    collection = "sentinel-s2-l2a-cogs"

    # Creating geometry file from inpiut location and size of area wanted
    km2lat = 1/110.574
    km2lon = 1/(111.320*np.cos(lat*np.pi/180))
    coords = []
    km = 5.12
    coords.append([lon-(km*0.5*km2lon), lat-(km*0.5*km2lat)])
    coords.append([lon+(km*0.5*km2lon), lat-(km*0.5*km2lat)])
    coords.append([lon+(km*0.5*km2lon), lat+(km*0.5*km2lat)])
    coords.append([lon-(km*0.5*km2lon), lat+(km*0.5*km2lat)])
    coords.append([lon-(km*0.5*km2lon), lat-(km*0.5*km2lat)])
    geom = {'type': "Polygon", "coordinates": [coords]}

    # Doing a search of all the satellite patches for that location (not necessarily having 100% intersection) for that date
    # having cloud coverage less than 1%. Then getting the last item from all the obtained patches
    mysearch = client.search(collections=[collection], intersects=geom, datetime=datetime, query=["eo:cloud_cover<1"])
    items = mysearch.get_all_items()
    assets = items[-1].assets

    # Getting the visual (rgb) image of the oatch obtained, and downloading it and saving in .tif format
    visual_href = assets["visual"].href
    visual = rioxarray.open_rasterio(visual_href)
    visual.rio.to_raster("current.tif", driver="COG")
    # Loading the saved .tif pacth image
    data = rasterio.open('./current.tif')

    # Getting the coordinate reference system for the data accessed using the API
    dcrs = data.crs

    # Getting rgb layers from the geotif file and bringing in correct order for display
    np_img = data.read([1, 2, 3])
    patch_img = np.moveaxis(np_img, 0, 2)

    # Projecting the geometry of the area we want into the coordinate refrence system same as that of the obtained patch
    geom_aoi = shape(geom)
    wgs84 = pyproj.CRS('EPSG:4326')
    utm = pyproj.CRS(str(dcrs))
    project = pyproj.Transformer.from_crs(wgs84, utm, always_xy=True).transform
    utm_point = transform(project, geom_aoi)
    geomk = geopandas.GeoSeries([utm_point])

    # Starting with showing map of the loation asked
    map = folium.Map([lat, lon], zoom_start=14)
    folium.GeoJson(utm_point).add_to(map)

    # Follwoing code has been taken from (https://notebooks.githubusercontent.com/view/ipynb?browser=unknown_browser&color_mode=auto&commit=7ffc87bae86867ceef21fbb80e514342054a319a&device=unknown_device&enc_url=68747470733a2f2f7261772e67697468756275736572636f6e74656e742e636f6d2f7368616b61736f6d2f72732d707974686f6e2d7475746f7269616c732f376666633837626165383638363763656566323166626238306535313433343230353461333139612f52535f507974686f6e2e6970796e62&logged_in=false&nwo=shakasom%2Frs-python-tutorials&path=RS_Python.ipynb&platform=unknown_platform&repository_id=194450692&repository_type=Repository&version=0)
    # Cropping out the desired image area using the transformed geometry file and the patch using rasterio.mask
    # Image is downloaded
    with rasterio.open("./current.tif") as src:
        out_image, out_transform = rasterio.mask.mask(src, geomk, crop=True)
        out_meta = src.meta.copy()
        out_meta.update({"driver": "GTiff",
                         "height": out_image.shape[1],
                         "width": out_image.shape[2],
                         "transform": out_transform})
    with rasterio.open("RGB_masked2.tif", "w", **out_meta) as dest:
        dest.write(out_image)

    # Loading the geotiff file of our desired area and converting it to numpy array
    # and into the form that can be displayed
    img_tiff = rasterio.open('./RGB_masked2.tif')
    np_img = img_tiff.read([1, 2, 3])
    img = np.moveaxis(np_img, 0, 2)

    return img, patch_img, map

# function starts


def predict(lt=25.6198, ln=85.2043, date1='2018-05-01', date2='2019-06-01'):

    # patna bridge

    slasheddate = "/".join([date1, date2])
    start_year = date1[:4]
    end_year = date2[:4]

    img, patch, map = get_satellite_img(
        lat=lt, lon=ln, datetime=slasheddate)

    years = [i for i in range(int(start_year), int(end_year)+1)]
    years = [str(i) for i in years]
    months = ['01','02','05','06','11','12']

    date_list, image_list = [], []
    for year in years:
        for month in months:
            start_date = year + '-' + month + '-' + '01'
            end_date = year + '-' + month + '-' + '28'
            date = start_date+'/'+end_date
            date_list.append(year + '-' + month)
            print(year + '-' + month)

            # Try to get the image for this date: If not then fill will NA
            try:
                img, patch, map = get_satellite_img(
                    lat=lt, lon=ln, datetime=date)
                image_list.append(img)
            except:
                image_list.append(None)

    # Creating the dataframe for every image of every date (even including the dates for which image is NA)
    df = pd.DataFrame([])
    df['Dates'] = date_list[:-3]
    df['Images'] = image_list[:-3]



    df.to_pickle('LandImages.pkl')
    df.to_csv("save1.csv")
    df = pd.read_pickle('LandImages.pkl')

    new_list = []
    for i in df.iloc[:, 1].values:
        if str(type(i)) != "<class 'NoneType'>":
            img_list = []
            img_list.append(i[:256, :256])
            img_list.append(i[:256, 256:512])
            img_list.append(i[256:512, :256])
            img_list.append(i[256:512, 256:512])
            new_list.append(img_list)
        else:
            new_list.append(None)
    df['Img_Tiled'] = new_list

    tiled_img = []
    for i in df['Img_Tiled'].values:
        if str(type(i)) != "<class 'NoneType'>":
            img_list = []
            for img in i:
                # Checking if the number of unique values in the image is less than 100, it will most probably be not a good image
                # Also choosing only the desired size and removing others.
                if img.shape == (256, 256, 3) or len(np.unique(img)) > 100:
                    img_list.append(img)
                else:
                    img_list.append(None)
            tiled_img.append(img_list)
        else:
            tiled_img.append(None)
    df['Img_Tiled_Filtered'] = tiled_img

    # Loading the pre-trained model
    model = tf.keras.models.load_model('./fyp_flask_server/ToDrive/model1.h5')  # global

    class_dict = []
    for image_list in df['Img_Tiled_Filtered'].values:
        if str(type(image_list)) != "<class 'NoneType'>":
            image_class_list = []
            for image in image_list:
                if str(type(image)) != "<class 'NoneType'>":
                    img = np.expand_dims(normalize(image), axis=0)
                    pred = model.predict(img)
                    class_pred = np.argmax(pred[0, :, :, :], axis=2)
                    classes, count = np.unique(class_pred, return_counts=True)[
                        0], np.unique(class_pred, return_counts=True)[1]
                    image_class_list.append([[i, 0] if (i not in classes) else [
                                            i, count[np.where(classes == i)[0][0]]] for i in range(7)])
                else:
                    image_class_list.append(
                        [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0]])
                mean = np.mean(np.array(image_class_list), axis=0)
            class_dict.append(mean)
        else:
            class_dict.append(None)

    final_df = pd.DataFrame([])
    final_df['Dates'] = df['Dates']
    final_df['Urban'] = [None if str(
        i) == 'None' else i[1][1] for i in class_dict]
    final_df['Agriculture'] = [None if str(
        i) == 'None' else i[2][1] for i in class_dict]
    final_df['Rangeland'] = [None if str(
        i) == 'None' else i[3][1] for i in class_dict]
    final_df['Forest'] = [None if str(
        i) == 'None' else i[4][1] for i in class_dict]
    final_df['Water'] = [None if str(
        i) == 'None' else i[5][1] for i in class_dict]
    final_df['BarrenLand'] = [None if str(
        i) == 'None' else i[6][1] for i in class_dict]
    final_df['Others'] = [None if str(
        i) == 'None' else i[0][1] for i in class_dict]

    df = final_df
    # df = df.drop([i for i in range(12)], axis=0)
    df.reset_index(inplace=True, drop=True)

    df_copy = df.copy()
    df.dropna(inplace=True)
    df.isna().sum()

    # scaler = StandardScaler().fit_transform(df.drop(['Dates'], axis=1)
    X_embedded = TSNE(n_components=2, learning_rate='auto', init='random',
                      perplexity=3, random_state=41).fit_transform(df.drop(['Dates'], axis=1))

    # ax[0].scatter(X_embedded[:,0], X_embedded[:,1])
    # ax[0].axes.xaxis.set_ticklabels([])
    # ax[0].axes.yaxis.set_ticklabels([])
    # ax[0].set_title('Dimensionality Reduced Dataset', fontsize=20)
    # ax[0].set_xlabel('TSNE Axis 1', fontsize=12)
    # ax[0].set_ylabel('TSNE Axis 2', fontsize=12)

    kmeans = KMeans(n_clusters=2, random_state=0).fit(X_embedded)

    X_embedded = TSNE(n_components=2, learning_rate='auto', init='random',
                      perplexity=3, random_state=41).fit_transform(df.drop(['Dates'], axis=1))

    kmeans = KMeans(n_clusters=2, random_state=0).fit(X_embedded)

    df['cluster'] = kmeans.labels_
    df_cluster_corrected = df.copy()
    for i in range(len(df_cluster_corrected)):
        if df_cluster_corrected['cluster'].values[i] == 1:
            df_cluster_corrected.iloc[i] = [df_cluster_corrected['Dates'].values[i], np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]
    df_cluster_corrected.drop(['cluster'], axis=1, inplace=True)
    df_cluster_corrected.reset_index(inplace=True, drop=True)

    # Performing KNN Imputation for all the NaN values (initially outliers)
    imputer = KNNImputer(n_neighbors=4, missing_values=np.nan)
    imputed_df = imputer.fit_transform(
    df_cluster_corrected.drop(['Dates'], axis=1))
    imputed_df = pd.DataFrame(imputed_df, columns=[
                              'Urban',	'Agriculture',	'Rangeland',	'Forest',	'Water',	'BarrenLand',	'Others'])
    imputed_df = pd.concat([df_cluster_corrected.Dates, imputed_df], axis=1)

    final_df = pd.DataFrame([], columns=['Dates', 'Urban',	'Agriculture',
                            'Rangeland',	'Forest',	'Water',	'BarrenLand',	'Others'])
    for i, date in enumerate(df_copy.Dates.values):
        if date in imputed_df.Dates.values:
            final_df.loc[i, :] = list(
                imputed_df[imputed_df['Dates'] == date].values[0])
        else:
            final_df.loc[i, :] = [date, np.nan, np.nan,
                                  np.nan, np.nan, np.nan, np.nan, np.nan]

    # Peforming KNN Imputation again: For the NaN values which happended because there was no satellite image for that month
    imputer = KNNImputer(n_neighbors=4, missing_values=np.nan)
    final_imputed_df = imputer.fit_transform(final_df.drop(['Dates'], axis=1))
    column_names=['Urban',	'Agriculture',	'Rangeland',	'Forest',	'Water',	'BarrenLand',	'Others']
    final_imputed_df = pd.DataFrame(final_imputed_df, columns=[
                                    'Urban',	'Agriculture',	'Rangeland',	'Forest',	'Water',	'BarrenLand',	'Others'])
    final_imputed_df = pd.concat([df_copy.Dates, final_imputed_df], axis=1)

    final_imputed_df[['Urban',	'Agriculture',	'Rangeland',	'Forest',	'Water',
                      'BarrenLand',	'Others']] = final_imputed_df.drop(['Dates'], axis=1)*100/65536
    final_imputed_df[['Urban',	'Agriculture',	'Rangeland',	'Forest',	'Water',	'BarrenLand',	'Others']] = final_imputed_df[[
        'Urban',	'Agriculture',	'Rangeland',	'Forest',	'Water',	'BarrenLand',	'Others']].round(decimals=2)

    print(final_df)
    mean = [np.mean(final_imputed_df[i].values) for i in final_imputed_df.columns[1:]]
    variance = [np.std(final_imputed_df[i].values) for i in final_imputed_df.columns[1:]]
    print(f"mean : {mean}, vari: {variance}")
    return {"mean":mean,"name":column_names}



# predict(date1= '2023-05-03', date2= '2023-05-04', lt= 25.516421116841073, ln= 85.80902727379186)
# predict(lt=25.6198, ln=85.2043, date1='2017-05-01', date2='2018-06-01')