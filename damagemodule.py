from PIL import Image
from numpy import asarray
# install tensorflow and related dependencies using pip3 install tensorflow and whatever else required
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model


def predict(model, img):
    numpydata = asarray(img)  # converting the image into array
    # reshaping the image array for the model
    p = numpydata.reshape(1, 128, 128, 3)
    y = model.predict(p)  # using the loaded model for prediction
    if y[0][0] > 0.5:  # using the probablity score to display the final output
        print("Not damaged")
        return 0
    else:
        print("Damaged")
        return 1
# if there are many images, a loop can be used for prediction or they can be grouped into one single array too.
