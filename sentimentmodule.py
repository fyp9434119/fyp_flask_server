from nltk.stem import SnowballStemmer
import numpy as np
import pandas as pd
import nltk
import tweepy
import spacy
from tensorflow.keras.utils import pad_sequences
import tensorflow as tf
import string
import re
API_KEY = "WBda0vaqRhwJG47iCaYQqF0ZM"
API_SECRET = "dn1U4Lydfw2WZb4N04aog4GIlW35KNYl6OVVMvSRNLZGrIUjQa"
BEARER_TOKEN = "AAAAAAAAAAAAAAAAAAAAADIljwEAAAAA4ymLx7v3st6dreTwnrUZFwKDOJs%3DPA76jKpbfKXNLGSFc2USTabGtIY0gURIcQNttKEElJLCHq9unk"
ACCESS_TOKEN = "1433114883684401152-mEj6H1zc2eEX7gc9KvLHmLGc5WL78V"
ACCESS_TOKEN_SECRET = "SGxTKROWrVbNAtdN8oH2ZpxqwpbS6k5ezmrljUHqCHNIT"


sp = spacy.load('en_core_web_sm')


nltk.download('punkt')


def get_tweets(n, query):

    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

    # Create API object
    api = tweepy.API(auth)

    # Define the search query
    # query = "Python"

    tweets = []
    # Iterate through all tweets containing the given query
    for tweet in tweepy.Cursor(api.search_tweets,
                               q=query,
                               lang="en",
                               tweet_mode='extended').items(n):
        # Print the tweet's text
        tweets.append(tweet.full_text)
    print(len(tweets))
    return (tweets)


def make_small(df_new):
    index = [j for i, j in zip(
        df_new['text'].values, df_new.index.values) if len(i.split(' ')) > 10]
    # The emotion part of a tweet is contained in the beginning itself so we will analyse only the 10 worded tweets
    df_new.drop(index, inplace=True)
    return df_new


def clean(text):

    all_stopwords = sp.Defaults.stop_words  # list of stopwords in spacy
    # removing numbers
    text = ''.join((item for item in text if not item.isdigit()))

    text = re.sub(r'#', '', text)  # removing #'s
    text = re.sub(r'https?:\/\/\S+', '', text)  # removing hyperlink
    text = re.sub('[^\s]*\.(com|org|net)\S*', '',
                  text)  # removing domain names
    # ewmoving punctuations
    text = text.translate(str.maketrans('', '', string.punctuation))
    tokens = text.lower()  # converting to lower letters
    # tokens = nltk.word_tokenize(tokens)
    # tokens_without_sw = [word for word in tokens if not word in all_stopwords]
    return tokens  # returning the processed word list


# Define a function to stem a list of words
def stem_words(words):
    stemmer = SnowballStemmer("english")  # Create a stemmer object
    # Apply stemming to each word in the list
    stemmed_words = [stemmer.stem(word) for word in words]
    return stemmed_words



def predict_sentiment(keyword, NumOfTweets, tokenizer, model):
    

    tweets = []
    # taking +1 as the loop will end one iteration before itself
    limit = (NumOfTweets+1)
    # for tweet in sntwitter.TwitterSearchScraper(query).get_items():  # extracting the tweets
    #   tweets.append([tweet.date,tweet.username,tweet.content]) # aooending them to a list to be converted into a dataframe
    #   if len(tweets)==limit:  # doing it till the require limit
    #     break
    tweets = get_tweets(limit, keyword)
    cols = ["text"]  # converting the list into dfs
    df_new = pd.DataFrame(tweets, columns=cols)
    df_new = df_new[['text']]  # final df of tweets extracted
    # cleaning the tweets with our clean function
    predict_df = df_new['text'].apply(lambda x: clean(x))
    final_dict = {0: 0, 1: 0, 2: 0}
    for i in predict_df:
        try:
            inputs = tokenizer(i, return_tensors="pt")
            outputs = model(**inputs)
            predicted_class = outputs.logits.argmax().item()
            
            if predicted_class == 1:
                final_dict[1] += 1
            elif predicted_class == 0:
                final_dict[0] += 1
            else:
                final_dict[2] += 1
        except:
            print("skipped")
        


    perc2 = (final_dict[2]/(final_dict[0]+final_dict[1]+final_dict[2]))*100
    # calculating perc of positive tweets
    perc1 = (final_dict[1]/(final_dict[0]+final_dict[1]+final_dict[2]))*100
    # calculating perc of negative tweets
    perc0 = (final_dict[0]/(final_dict[0]+final_dict[1]+final_dict[2]))*100
    return {"pos": perc2, "nut": perc1, "neg": perc0}
